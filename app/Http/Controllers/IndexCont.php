<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IndexCont extends Controller
{
    public function show()
    {
        $data = DB::table('mahasiswa')->count();
        return view('index', ['data' => $data]);
    }
}
