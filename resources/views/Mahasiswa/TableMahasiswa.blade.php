@extends('Layout.main')

@section('head')
    <h1 class="m-0">Data Mahasiswa</h1>
@endsection

@section('sidebar')
<div class="sidebar">
    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item menu-open">
          <a href="/" class="nav-link">
            <i class="nav-icon fas fa-home"></i>
            <p>
              Dashboard
            </p>
          </a>
        </li>
        <li class="nav-item menu-open">
          <a href="/mahasiswa" class="nav-link active">
            <i class="nav-icon fas fa-user-graduate"></i>
            <p>
              Data Mahasiswa
            </p>
          </a>
        </li>
        <li class="nav-item menu-open">
          <a href="/Buku" class="nav-link">
            <i class="nav-icon fas fa-book"></i>
            <p>
              Data Buku
            </p>
          </a>
        </li>
        <li class="nav-item menu-open">
          <a href="/Peminjaman" class="nav-link">
            <i class="nav-icon far fa-window-restore"></i>
            <p>
              Data Peminjaman
            </p>
          </a>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="card">
                  <a href="/FormMahasiswa" class="btn btn-success mt-2 mx-3 col-2"><i class="fas fa-plus-circle"></i>   Tambah Data</a>
                <div class="card-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama Mahasiswa</th>
                      <th>NIM</th>
                      <th>Email</th>
                      <th>No Telepon</th>
                      <th>Program Studi</th>
                      <th>Jurusan</th>
                      <th>Fakultas</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <?php $i=1;?>
                    <tbody>
                        @foreach ($data as $datas)
                            <tr>
                                <td><?php echo($i); $i++; ?></td>
                                <td>{{$datas->nama}}</td>
                                <td>{{$datas->nim}}</td>
                                <td>{{$datas->email}}</td>
                                <td>{{$datas->no_telp}}</td>
                                <td>{{$datas->prodi}}</td>
                                <td>{{$datas->jurusan}}</td>
                                <td>{{$datas->fakultas}}</td>
                                <td>
                                    <a href="/Edit{{$datas->id_mahasiswa}}" class="btn btn-warning"><i class="fas fa-edit"></i></a> 
                                    <a href="/delete{{$datas->id_mahasiswa}}" class="btn btn-danger"><i class="fas fa-trash-alt"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
          </div>
        </div>
      </section>
@endsection