@extends('Layout.main')

@section('head')
    <h1 class="m-0">Dashboard</h1>
@endsection

@section('sidebar')
<div class="sidebar">
    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item menu-open">
          <a href="/" class="nav-link active">
            <i class="nav-icon fas fa-home"></i>
            <p>
              Dashboard
            </p>
          </a>
        </li>
        <li class="nav-item menu-open">
          <a href="/mahasiswa" class="nav-link">
            <i class="nav-icon fas fa-user-graduate"></i>
            <p>
              Data Mahasiswa
            </p>
          </a>
        </li>
        <li class="nav-item menu-open">
          <a href="/buku" class="nav-link">
            <i class="nav-icon fas fa-book"></i>
            <p>
              Data Buku
            </p>
          </a>
        </li>
        <li class="nav-item menu-open">
          <a href="/peminjaman" class="nav-link">
            <i class="nav-icon far fa-window-restore"></i>
            <p>
              Data Peminjaman
            </p>
          </a>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-info">
                <div class="inner">
                  <h3>{{$data}}</h3>
  
                  <p>Data Mahasiswa</p>
                </div>
                <div class="icon">
                  <i class="fas fa-user-graduate"></i>
                </div>
                <a href="#" class="small-box-footer">Selengkapnya  <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-success">
                <div class="inner">
                  <h3>53</h3>
                  <p>Data Buku</p>
                </div>
                <div class="icon">
                  <i class="fas fa-book"></i>
                </div>
                <a href="#" class="small-box-footer">Selengkapnya  <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-warning">
                <div class="inner">
                  <h3>44</h3>
                  <p>Data Peminjaman</p>
                </div>
                <div class="icon">
                  <i class="far fa-window-restore"></i>
                </div>
                <a href="#" class="small-box-footer">Selengkapnya  <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
@endsection